import 'dart:io';

import 'repository.dart';

abstract class FirebaseStorageRepository extends Repository {
  /// Return file url
  Future<String> uploadFile(File file);
}
