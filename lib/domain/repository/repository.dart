import 'dart:io';

import 'package:path_provider/path_provider.dart';

import '../../data/firebase/firebase_storage_folders.dart';

abstract class Repository {
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _userJsonFile async {
    final path = await _localPath;
    return File('$path/users.json');
  }

  Future<File> get getUserJsonFile async {
    final file = await _userJsonFile;
    if (file.existsSync()) {
      return file;
    } else {
      file.createSync(recursive: true);
      return file;
    }
  }

  Future<File> get _userXmlFile async {
    final path = await _localPath;
    return File('$path/users.xml');
  }

  Future<File> get getUserXmlFile async {
    final file = await _userXmlFile;
    if (file.existsSync()) {
      return file;
    } else {
      file.createSync(recursive: true);
      return file;
    }
  }

  FirebaseStorageFolders get firebaseStorageFolders => FirebaseStorageFolders();
}
