import '../../data/models/user_model.dart';
import 'repository.dart';

abstract class UserRepository extends Repository {
  Future<List<UserModel>> readUserJsonFile();

  Future<void> writeUserJsonFile(List<UserModel> users);

  Future<List<UserModel>> readUserXmlFile();

  Future<void> writeUserXmlFile(List<UserModel> users);
}
