class UserModel {
  final int id;
  final String name;
  final String phoneNumber;
  final String address;
  final DateTime birthday;
  final Gender gender;
  final DateTime createAt;
  final FileSaveFormat fileSaveFormat;

  const UserModel({
    required this.id,
    required this.name,
    required this.phoneNumber,
    required this.address,
    required this.birthday,
    required this.gender,
    required this.createAt,
    this.fileSaveFormat = FileSaveFormat.json,
  });

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "name": name,
      "phoneNumber": phoneNumber,
      "address": address,
      "birthday": birthday.toIso8601String(),
      "gender": gender.name,
      "createAt": createAt.toIso8601String(),
      "fileSaveFormat": fileSaveFormat.name,
    };
  }

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      id: json["id"] as int,
      name: json["name"] as String,
      phoneNumber: json["phoneNumber"] as String,
      address: json["address"] as String,
      birthday: DateTime.parse(json['birthday'] as String),
      gender: Gender.values.byName(json["gender"] as String),
      createAt: DateTime.parse(json['createAt'] as String),
      fileSaveFormat:
          FileSaveFormat.values.byName(json["fileSaveFormat"] as String),
    );
  }

  UserModel copyWith({
    int? id,
    String? name,
    String? phoneNumber,
    String? address,
    DateTime? birthday,
    Gender? gender,
    DateTime? createAt,
    FileSaveFormat? fileSaveFormat,
  }) {
    return UserModel(
      id: id ?? this.id,
      name: name ?? this.name,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      address: address ?? this.address,
      birthday: birthday ?? this.birthday,
      gender: gender ?? this.gender,
      createAt: createAt ?? this.createAt,
      fileSaveFormat: fileSaveFormat ?? this.fileSaveFormat,
    );
  }

  String toRecordTsv() {
    return '$id\t$name\t$phoneNumber\t"$address"\t${birthday.toIso8601String()}\t${gender.name}\t${createAt.toIso8601String()}\t${fileSaveFormat.name}';
  }

  factory UserModel.fromRecordTsv(String record) {
    List<String> values = record.split('\t');

    return UserModel(
      id: int.parse(values[0].trim()),
      name: values[1].trim(),
      phoneNumber: values[2].trim(),
      address: values[3].trim(),
      birthday: DateTime.parse(values[4].trim()),
      gender: Gender.values.byName(values[5].trim()),
      createAt: DateTime.parse(values[6].trim()),
      fileSaveFormat: FileSaveFormat.values.byName(values[7].trim()),
    );
  }
}

enum Gender {
  male,
  female,
}

extension GenderEx on Gender {
  bool get isMale => this == Gender.male;
  bool get isFemale => this == Gender.female;

  String get text {
    if (isMale) {
      return 'Nam';
    }
    if (isFemale) {
      return 'Nữ';
    }
    return 'Nam';
  }
}

enum FileSaveFormat {
  json,
  xml,
}

extension FileSaveFormatEx on FileSaveFormat {
  bool get isJson => this == FileSaveFormat.json;
  bool get isXml => this == FileSaveFormat.xml;
}
