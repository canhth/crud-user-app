import 'models/user_model.dart';

List<UserModel> randomUserList = [
  UserModel(
    id: 1,
    name: 'Sơn Tùng MTP',
    phoneNumber: '0123456789',
    address: 'TP. Thái Bình',
    birthday: DateTime(1994, 7, 5),
    gender: Gender.male,
    createAt: DateTime.now(),
    fileSaveFormat: FileSaveFormat.xml,
  ),
  UserModel(
    id: 2,
    name: 'Hoàng Thuỳ Linh',
    phoneNumber: '0999999999',
    address: 'Hà Nội',
    birthday: DateTime(1988, 8, 11),
    gender: Gender.female,
    createAt: DateTime.now(),
    fileSaveFormat: FileSaveFormat.json,
  ),
];
