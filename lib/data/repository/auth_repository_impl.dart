import '../../domain/repository/repository.dart';

abstract class AuthRepository extends Repository {
  Future<void> loginWithZalo();
}

class AuthRepositoryImpl extends AuthRepository {
  @override
  Future<void> loginWithZalo() {
    // TODO: implement loginWithZalo
    throw UnimplementedError();
  }
}
