import 'dart:io';

import 'package:firebase_core/firebase_core.dart';

import '../../domain/repository/firebase_storage_repository.dart';
import '../tsv_file/file_manager.dart';

class FirebaseStorageRepositoryImpl extends FirebaseStorageRepository {
  @override
  Future<String> uploadFile(File file) async {
    try {
      final fileName = FileManager().getFileName(file);

      final ref = firebaseStorageFolders.tsvFileFolder.child('/$fileName');

      await ref.putFile(file);

      return await ref.getDownloadURL();
    } on FirebaseException {
      rethrow;
    } catch (e) {
      rethrow;
    }
  }
}
