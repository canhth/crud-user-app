import 'dart:convert';

import 'package:xml/xml.dart' as xml;

import '../../domain/repository/user_repository.dart';
import '../models/user_model.dart';

class UserRepositoryImpl extends UserRepository {
  @override
  Future<List<UserModel>> readUserJsonFile() async {
    try {
      final file = await getUserJsonFile;

      final contents = file.readAsStringSync();

      if (contents.isEmpty) {
        return [];
      }

      final List<dynamic> jsonList = jsonDecode(contents);

      return jsonList.map((json) => UserModel.fromJson(json)).toList();
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<void> writeUserJsonFile(List<UserModel> users) async {
    try {
      final file = await getUserJsonFile;

      final contents = jsonEncode(users);

      file.writeAsStringSync(contents);
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<UserModel>> readUserXmlFile() async {
    try {
      final file = await getUserXmlFile;
      final xmlString = file.readAsStringSync();

      if (xmlString.isEmpty) {
        return [];
      }

      final xmlDoc = xml.XmlDocument.parse(xmlString);
      final userElements = xmlDoc.findAllElements('user').toList();

      return userElements.map(
        (xml.XmlElement userElement) {
          /// Data is not null
          final id = userElement.getElement('id')!.text;
          final name = userElement.getElement('name')!.text;
          final phoneNumber = userElement.getElement('phoneNumber')!.text;
          final address = userElement.getElement('address')!.text;
          final birthday = userElement.getElement('birthday')!.text;
          final gender = userElement.getElement('gender')!.text;
          final createAt = userElement.getElement('createAt')!.text;
          final fileSaveFormat = userElement.getElement('fileSaveFormat')!.text;

          return UserModel(
            id: int.parse(id),
            name: name,
            phoneNumber: phoneNumber,
            address: address,
            birthday: DateTime.parse(birthday),
            gender: Gender.values.byName(gender),
            createAt: DateTime.parse(createAt),
            fileSaveFormat: FileSaveFormat.values.byName(fileSaveFormat),
          );
        },
      ).toList();
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<void> writeUserXmlFile(List<UserModel> users) async {
    try {
      final builder = xml.XmlBuilder();
      builder.processing('xml', 'version="1.0"');
      builder.element('users', nest: () {
        for (final user in users) {
          builder.element('user', nest: () {
            builder.element('id', nest: user.id);
            builder.element('name', nest: user.name);
            builder.element('phoneNumber', nest: user.phoneNumber);
            builder.element('address', nest: user.address);
            builder.element('birthday', nest: user.birthday.toIso8601String());
            builder.element('gender', nest: user.gender.name);
            builder.element('createAt', nest: user.createAt.toIso8601String());
            builder.element('fileSaveFormat', nest: user.fileSaveFormat.name);
          });
        }
      });

      final xmlDoc = builder.buildDocument();
      final xmlString = xmlDoc.toXmlString(pretty: true);

      final file = await getUserXmlFile;
      file.writeAsStringSync(xmlString);
    } catch (e) {
      rethrow;
    }
  }
}
