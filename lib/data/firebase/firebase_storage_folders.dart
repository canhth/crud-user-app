import 'package:firebase_storage/firebase_storage.dart';

class FirebaseStorageFoldersName {
  const FirebaseStorageFoldersName._init();

  static const tsvFile = 'tsv_files';
}

class FirebaseStorageFolders {
  final _storageRef = FirebaseStorage.instance.ref();

  Reference get tsvFileFolder =>
      _storageRef.child(FirebaseStorageFoldersName.tsvFile);
}
