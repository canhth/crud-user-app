import 'dart:io';

import 'package:path_provider/path_provider.dart';

import '../../presentation/constants/constants.dart';
import '../../presentation/utils/permission_handler.dart';
import '../models/user_model.dart';
import 'tsv_file_manager.dart';

class UserTsvFileManager extends TsvFileManager<UserModel> {
  @override
  Future<File> createTempTsvFile(
    String fileName,
    List<UserModel> users,
  ) async {
    try {
      /// Check storage permission
      await PermissionHandler().checkStoragePermission();

      Directory directory = await getTemporaryDirectory();

      final file = await File('${directory.path}/$fileName.tsv').create();

      final tsvContent = users
          .map((user) => user.toRecordTsv())
          .join(Constants.tsvRecordSeparatorCharacter);

      await file.writeAsString(tsvContent);
      return file;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<UserModel>> readTsvFile(File file) async {
    try {
      final tsvContent = await file.readAsString();

      if (tsvContent.isEmpty) {
        return [];
      }

      return tsvContent
          .split(Constants.tsvRecordSeparatorCharacter)
          .map((record) => UserModel.fromRecordTsv(record.trim()))
          .toList();
    } catch (e) {
      rethrow;
    }
  }
}
