import 'dart:io';

abstract class TsvFileManager<T> {
  Future<File> createTempTsvFile(String fileName, List<T> users);

  Future<List<T>> readTsvFile(File file);
}
