import 'dart:io';

import 'package:dio/dio.dart';
import 'package:open_filex/open_filex.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share_plus/share_plus.dart';

import '../../presentation/constants/constants.dart';
import '../../presentation/utils/permission_handler.dart';

class FileManager {
  static final _singleton = FileManager._internal();
  FileManager._internal();

  factory FileManager() => _singleton;

  /// Example: 'file.tsv'
  String getFileName(File file) {
    return file.path.split('/').last;
  }

  /// Example: tsv
  String getFileType(File file) {
    final fileName = getFileName(file);

    return fileName.split('.').last;
  }

  Future<File> saveFileToDownloadFolder(File file) async {
    try {
      await PermissionHandler().checkStoragePermission();

      Directory directory = Directory("");

      if (Platform.isAndroid) {
        directory = Directory(Constants.downloadDirectoryPathOnAndroid);
      } else {
        directory = await getApplicationDocumentsDirectory();
      }

      final fileName = getFileName(file);

      final fileSaved = await file.copy('${directory.path}/$fileName.tsv');

      return fileSaved;
    } catch (e) {
      rethrow;
    }
  }

  Future<void> openFile(File file) async {
    try {
      if (file.existsSync()) {
        await OpenFilex.open(file.path);
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<void> shareFile(
    File file, {
    String title = 'File user list',
  }) async {
    try {
      XFile xfile = XFile(file.path);
      await Share.shareXFiles(
        [xfile],
        subject: title,
      );
    } catch (e) {
      rethrow;
    }
  }

  Future<File> downloadFile(String url) async {
    try {
      Directory tempDir = await getTemporaryDirectory();

      final fileName = 'users_${DateTime.now().millisecondsSinceEpoch}.tsv';

      final filePath = '${tempDir.path}/$fileName';

      await Dio().download(url, filePath);

      File file = File(filePath);

      return file;
    } catch (e) {
      rethrow;
    }
  }
}
