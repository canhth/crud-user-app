enum ImportExportStatus { initial, loading, success, failure, cancel }

extension ImportExportStatusEx on ImportExportStatus {
  bool get isInitial => this == ImportExportStatus.initial;
  bool get isLoading => this == ImportExportStatus.loading;
  bool get isSuccess => this == ImportExportStatus.success;
  bool get isFailure => this == ImportExportStatus.failure;
  bool get isCancel => this == ImportExportStatus.cancel;
}
