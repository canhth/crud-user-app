import 'package:permission_handler/permission_handler.dart';

class PermissionHandler {
  static final _singleton = PermissionHandler._internal();
  PermissionHandler._internal();

  factory PermissionHandler() => _singleton;

  /// Check storage permission
  Future<bool> checkStoragePermission() async {
    var status = await Permission.storage.status;

    if (!status.isGranted) {
      status = await Permission.storage.request();
      if (!status.isGranted) {
        return false;
      }
    }

    return true;
  }
}
