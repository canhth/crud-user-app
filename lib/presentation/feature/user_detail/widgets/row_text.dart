import 'package:flutter/material.dart';

class RowText extends StatelessWidget {
  const RowText({
    Key? key,
    required this.title,
    required this.content,
  }) : super(key: key);

  final String title;
  final String content;

  @override
  Widget build(BuildContext context) {
    const textStyle = TextStyle(
      fontSize: 20,
    );

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Row(
        children: [
          Text(
            '$title: ',
            style: textStyle,
          ),
          const SizedBox(width: 10),
          Text(
            content,
            style: textStyle,
          )
        ],
      ),
    );
  }
}
