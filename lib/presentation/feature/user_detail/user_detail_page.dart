import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../../../data/models/user_model.dart';
import '../../../domain/repository/user_repository.dart';
import '../edit_user/edit_user_page.dart';
import 'widgets/row_text.dart';

class UserDetailPage extends StatefulWidget {
  const UserDetailPage({
    Key? key,
    required this.user,
  }) : super(key: key);

  final UserModel user;

  @override
  State<UserDetailPage> createState() => _UserDetailPageState();
}

class _UserDetailPageState extends State<UserDetailPage> {
  late UserModel user;

  @override
  void initState() {
    super.initState();

    user = widget.user;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Chi tiết người dùng'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            RowText(title: 'Họ tên', content: user.name),
            RowText(title: 'Giới tính', content: user.gender.text),
            RowText(
              title: 'Ngày sinh',
              content: DateFormat.yMd('vi_VN').format(user.birthday),
            ),
            RowText(title: 'Số ĐT', content: user.phoneNumber),
            RowText(title: 'Địa chỉ', content: user.address),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => EditUserPage(user: user),
            ),
          );

          final userUpdated = await _refreshUser(user);

          setState(() {
            user = userUpdated;
          });
        },
        child: const Icon(Icons.edit),
      ),
    );
  }

  Future<UserModel> _refreshUser(UserModel user) async {
    final List<UserModel> userList;
    if (user.fileSaveFormat.isJson) {
      userList = await context.read<UserRepository>().readUserJsonFile();
    } else {
      userList = await context.read<UserRepository>().readUserXmlFile();
    }

    return userList.firstWhere((e) => e.id == user.id);
  }
}
