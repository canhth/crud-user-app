import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../data/models/user_model.dart';
import '../../../../domain/repository/user_repository.dart';

part 'edit_user_event.dart';
part 'edit_user_state.dart';

class EditUserBloc extends Bloc<EditUserEvent, EditUserState> {
  EditUserBloc({
    required UserRepository userRepository,
    UserModel? user,
  })  : _userRepository = userRepository,
        super(user != null ? EditUserState.user(user) : const EditUserState()) {
    on<EditUserNameChanged>(_onNameChanged);
    on<EditUserPhoneNumberChanged>(_onPhoneNumberChanged);
    on<EditUserAddressChanged>(_onAddressChanged);
    on<EditUserBirthdayChanged>(_onBirthdayChanged);
    on<EditUserGenderChanged>(_onGenderChanged);
    on<EditUserFileSaveFormatChanged>(_onFileSaveFormatChanged);
    on<EditUserSaved>(_onUserSaved);
  }

  final UserRepository _userRepository;

  FutureOr<void> _onNameChanged(
    EditUserNameChanged event,
    Emitter<EditUserState> emit,
  ) {
    emit(state.copyWith(name: event.name));
  }

  FutureOr<void> _onPhoneNumberChanged(
    EditUserPhoneNumberChanged event,
    Emitter<EditUserState> emit,
  ) {
    emit(state.copyWith(phoneNumber: event.phoneNumber));
  }

  FutureOr<void> _onAddressChanged(
    EditUserAddressChanged event,
    Emitter<EditUserState> emit,
  ) {
    emit(state.copyWith(address: event.address));
  }

  FutureOr<void> _onBirthdayChanged(
    EditUserBirthdayChanged event,
    Emitter<EditUserState> emit,
  ) {
    emit(state.copyWith(birthday: event.birthday));
  }

  FutureOr<void> _onGenderChanged(
    EditUserGenderChanged event,
    Emitter<EditUserState> emit,
  ) {
    emit(state.copyWith(gender: event.gender));
  }

  FutureOr<void> _onFileSaveFormatChanged(
    EditUserFileSaveFormatChanged event,
    Emitter<EditUserState> emit,
  ) {
    emit(state.copyWith(fileSaveFormat: event.fileSaveFormat));
  }

  FutureOr<void> _onUserSaved(
    EditUserSaved event,
    Emitter<EditUserState> emit,
  ) async {
    emit(state.copyWith(status: EditUserStatus.loading));

    if (state.birthday == null) {
      return;
    }

    final userUpdated = state.isAddUser
        ? UserModel(
            id: DateTime.now().millisecondsSinceEpoch,
            name: state.name ?? '',
            phoneNumber: state.phoneNumber ?? '',
            address: state.address ?? '',
            birthday: state.birthday!,
            gender: state.gender,
            createAt: DateTime.now(),
            fileSaveFormat: state.fileSaveFormat,
          )
        : state.initialUser!.copyWith(
            name: state.name,
            phoneNumber: state.phoneNumber,
            address: state.address,
            birthday: state.birthday,
            gender: state.gender,
          );

    try {
      late final List<UserModel> oldUserList;
      late final List<UserModel> newUserList;

      if (userUpdated.fileSaveFormat.isJson) {
        oldUserList = await _userRepository.readUserJsonFile();

        if (state.isAddUser) {
          newUserList = [...oldUserList, userUpdated];
        } else {
          newUserList = [
            for (final user in oldUserList)
              user.id == userUpdated.id ? userUpdated : user
          ];
        }

        await _userRepository.writeUserJsonFile(newUserList);
      } else {
        oldUserList = await _userRepository.readUserXmlFile();

        if (state.isAddUser) {
          newUserList = [...oldUserList, userUpdated];
        } else {
          newUserList = [
            for (final user in oldUserList)
              user.id == userUpdated.id ? userUpdated : user
          ];
        }

        await _userRepository.writeUserXmlFile(newUserList);
      }

      emit(state.copyWith(status: EditUserStatus.success));
    } catch (e) {
      emit(state.copyWith(status: EditUserStatus.failure));
    }
  }
}
