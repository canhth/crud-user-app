part of 'edit_user_bloc.dart';

abstract class EditUserEvent {
  const EditUserEvent();
}

class EditUserNameChanged extends EditUserEvent {
  const EditUserNameChanged(this.name);

  final String name;
}

class EditUserPhoneNumberChanged extends EditUserEvent {
  const EditUserPhoneNumberChanged(this.phoneNumber);

  final String phoneNumber;
}

class EditUserAddressChanged extends EditUserEvent {
  const EditUserAddressChanged(this.address);

  final String address;
}

class EditUserGenderChanged extends EditUserEvent {
  const EditUserGenderChanged(this.gender);

  final Gender gender;
}

class EditUserBirthdayChanged extends EditUserEvent {
  const EditUserBirthdayChanged(this.birthday);

  final DateTime birthday;
}

class EditUserFileSaveFormatChanged extends EditUserEvent {
  const EditUserFileSaveFormatChanged(this.fileSaveFormat);

  final FileSaveFormat fileSaveFormat;
}

class EditUserSaved extends EditUserEvent {
  const EditUserSaved();
}
