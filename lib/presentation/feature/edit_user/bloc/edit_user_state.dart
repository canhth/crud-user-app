part of 'edit_user_bloc.dart';

enum EditUserStatus { initial, loading, success, failure }

extension EditUserStatusEx on EditUserStatus {
  bool get isInitial => this == EditUserStatus.initial;
  bool get isLoading => this == EditUserStatus.loading;
  bool get isSuccess => this == EditUserStatus.success;
  bool get isFailure => this == EditUserStatus.failure;
}

class EditUserState {
  final EditUserStatus status;
  final UserModel? initialUser;
  final String? name;
  final String? phoneNumber;
  final String? address;
  final DateTime? birthday;
  final Gender gender;
  final FileSaveFormat fileSaveFormat;

  bool get isAddUser => initialUser == null;

  const EditUserState({
    this.status = EditUserStatus.initial,
    this.initialUser,
    this.name,
    this.phoneNumber,
    this.birthday,
    this.address,
    this.gender = Gender.male,
    this.fileSaveFormat = FileSaveFormat.json,
  });

  factory EditUserState.user(UserModel user) => EditUserState(
        status: EditUserStatus.initial,
        initialUser: user,
        name: user.name,
        phoneNumber: user.phoneNumber,
        gender: user.gender,
        address: user.address,
        birthday: user.birthday,
        fileSaveFormat: user.fileSaveFormat,
      );

  EditUserState copyWith({
    EditUserStatus? status,
    UserModel? initialUser,
    String? name,
    String? phoneNumber,
    String? address,
    DateTime? birthday,
    Gender? gender,
    FileSaveFormat? fileSaveFormat,
  }) {
    return EditUserState(
      status: status ?? this.status,
      initialUser: initialUser ?? this.initialUser,
      name: name ?? this.name,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      address: address ?? this.address,
      birthday: birthday ?? this.birthday,
      gender: gender ?? this.gender,
      fileSaveFormat: fileSaveFormat ?? this.fileSaveFormat,
    );
  }
}
