import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../bloc/edit_user_bloc.dart';

class BirthdayInput extends StatelessWidget {
  const BirthdayInput({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12.0),
      child: Row(
        children: [
          const Text(
            'Ngày sinh',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
          ),
          BlocBuilder<EditUserBloc, EditUserState>(
            buildWhen: (previous, current) {
              return previous.birthday != current.birthday;
            },
            builder: (context, state) {
              return OutlinedButton(
                onPressed: () async {
                  final date = await showDatePicker(
                    context: context,
                    initialDate: state.birthday ?? DateTime.now(),
                    firstDate: DateTime(1000),
                    lastDate: DateTime(3000),
                  );

                  if (date != null && context.mounted) {
                    context
                        .read<EditUserBloc>()
                        .add(EditUserBirthdayChanged(date));
                  }
                },
                child: Text(
                  state.birthday != null
                      ? DateFormat.yMd().format(state.birthday!)
                      : 'Chọn ngày sinh',
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
