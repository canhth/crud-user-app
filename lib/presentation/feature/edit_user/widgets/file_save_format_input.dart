import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../data/models/user_model.dart';
import '../bloc/edit_user_bloc.dart';

class FileSaveFormatInput extends StatelessWidget {
  const FileSaveFormatInput({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 12.0),
        const Text(
          'Chọn định dạng lưu',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
        ),
        BlocBuilder<EditUserBloc, EditUserState>(
          buildWhen: (previous, current) {
            return previous.fileSaveFormat != current.fileSaveFormat;
          },
          builder: (context, state) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(
                  child: RadioListTile(
                    value: FileSaveFormat.json,
                    title: Text(FileSaveFormat.json.name),
                    groupValue: state.fileSaveFormat,
                    onChanged: state.isAddUser
                        ? (FileSaveFormat? value) {
                            if (value != null) {
                              context
                                  .read<EditUserBloc>()
                                  .add(EditUserFileSaveFormatChanged(value));
                            }
                          }
                        : null,
                  ),
                ),
                Expanded(
                  child: RadioListTile(
                    value: FileSaveFormat.xml,
                    title: Text(FileSaveFormat.xml.name),
                    groupValue: state.fileSaveFormat,
                    onChanged: state.isAddUser
                        ? (FileSaveFormat? value) {
                            if (value != null) {
                              context
                                  .read<EditUserBloc>()
                                  .add(EditUserFileSaveFormatChanged(value));
                            }
                          }
                        : null,
                  ),
                )
              ],
            );
          },
        ),
      ],
    );
  }
}
