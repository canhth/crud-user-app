import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/edit_user_bloc.dart';
import 'input_field.dart';

class PhoneNumberInput extends StatelessWidget {
  const PhoneNumberInput({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EditUserBloc, EditUserState>(
      buildWhen: (previous, current) {
        return previous.phoneNumber != current.phoneNumber;
      },
      builder: (context, state) {
        return InputField(
          title: 'Số điện thoại',
          maxLength: 10,
          initialText: state?.phoneNumber,
          onChanged: (value) {
            context.read<EditUserBloc>().add(EditUserPhoneNumberChanged(value));
          },
        );
      },
    );
  }
}
