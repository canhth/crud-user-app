import 'package:crud_user_app/data/models/user_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/edit_user_bloc.dart';

class GenderInput extends StatelessWidget {
  const GenderInput({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 12.0),
        const Text(
          'Giới tính',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
        ),
        BlocBuilder<EditUserBloc, EditUserState>(
          buildWhen: (previous, current) {
            return previous.gender != current.gender;
          },
          builder: (context, state) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(
                  child: RadioListTile(
                    value: Gender.male,
                    title: Text(Gender.male.text),
                    groupValue: state.gender,
                    onChanged: (value) {
                      if (value != null) {
                        context
                            .read<EditUserBloc>()
                            .add(EditUserGenderChanged(value));
                      }
                    },
                  ),
                ),
                Expanded(
                  child: RadioListTile(
                    value: Gender.female,
                    title: Text(Gender.female.text),
                    groupValue: state.gender,
                    onChanged: (value) {
                      if (value != null) {
                        context
                            .read<EditUserBloc>()
                            .add(EditUserGenderChanged(value));
                      }
                    },
                  ),
                )
              ],
            );
          },
        ),
      ],
    );
  }
}
