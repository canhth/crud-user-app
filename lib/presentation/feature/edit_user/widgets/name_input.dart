import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/edit_user_bloc.dart';
import 'input_field.dart';

class NameInput extends StatelessWidget {
  const NameInput({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EditUserBloc, EditUserState>(
      buildWhen: (previous, current) {
        return previous.name != current.name;
      },
      builder: (context, state) {
        return InputField(
          margin: EdgeInsets.zero,
          title: 'Họ tên',
          maxLength: 40,
          initialText: state?.name,
          onChanged: (value) {
            context.read<EditUserBloc>().add(EditUserNameChanged(value));
          },
        );
      },
    );
  }
}
