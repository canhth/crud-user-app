import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/edit_user_bloc.dart';
import 'input_field.dart';

class AddressInput extends StatelessWidget {
  const AddressInput({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EditUserBloc, EditUserState>(
      buildWhen: (previous, current) {
        return previous.address != current.address;
      },
      builder: (context, state) {
        return InputField(
          title: 'Địa chỉ',
          maxLength: 40,
          initialText: state?.address,
          onChanged: (value) {
            context.read<EditUserBloc>().add(EditUserAddressChanged(value));
          },
        );
      },
    );
  }
}
