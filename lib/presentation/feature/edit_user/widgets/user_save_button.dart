import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/edit_user_bloc.dart';

class UserSaveButton extends StatelessWidget {
  const UserSaveButton({
    super.key,
    required GlobalKey<FormState> formKey,
  }) : _formKey = formKey;

  final GlobalKey<FormState> _formKey;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EditUserBloc, EditUserState>(
      builder: (context, state) {
        return FloatingActionButton(
          onPressed: () {
            if (state.birthday == null) {
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  const SnackBar(content: Text('Bạn chưa thêm ngày sinh.')),
                );

              return;
            }

            if (_formKey.currentState!.validate()) {
              context.read<EditUserBloc>().add(const EditUserSaved());
            }
          },
          child: const Icon(Icons.save),
        );
      },
    );
  }
}
