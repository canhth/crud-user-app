import 'package:flutter/material.dart';

class InputField extends StatefulWidget {
  const InputField({
    Key? key,
    required this.title,
    this.initialText,
    this.maxLength,
    this.onChanged,
    this.controller,
    this.autoDisposeController = true,
    this.showCounterText = false,
    this.margin = const EdgeInsets.only(top: 12.0),
  }) : super(key: key);

  /// Title of the input field.
  final String title;

  /// Initial text of the input field.
  final String? initialText;

  /// Maximum length of input characters
  final int? maxLength;

  /// Callback when text has been changed
  /// return error text
  final ValueChanged<String>? onChanged;

  /// Controller for this Input Field
  final TextEditingController? controller;

  /// Auto Dispose Controller
  final bool autoDisposeController;

  /// Whether counter text enabled or not
  final bool showCounterText;

  final EdgeInsets? margin;

  @override
  State<InputField> createState() => _InputFieldState();
}

class _InputFieldState extends State<InputField> {
  late TextEditingController _textEditingController;

  @override
  void initState() {
    super.initState();

    _textEditingController = widget.controller ?? TextEditingController();

    if (widget.initialText != null &&
        _textEditingController.text != widget.initialText!) {
      _textEditingController.text = widget.initialText!;
    }
  }

  @override
  void dispose() {
    if (widget.autoDisposeController) {
      _textEditingController.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: widget.margin,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.title,
            style: const TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
          ),
          _buildTextFormField(context),
        ],
      ),
    );
  }

  Widget _buildTextFormField(BuildContext context) {
    return TextFormField(
      key: widget.key != null ? ValueKey('${widget.key}TextFormField') : null,
      controller: _textEditingController,
      maxLength: widget.maxLength,
      onChanged: widget.onChanged,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.all(16),
        counterText: widget.showCounterText ? null : '',
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Trường này không được trống.';
        }
        return null;
      },
    );
  }
}
