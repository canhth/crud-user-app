import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/models/user_model.dart';
import '../../../domain/repository/user_repository.dart';
import 'bloc/edit_user_bloc.dart';
import 'widgets/address_input.dart';
import 'widgets/birthday_input.dart';
import 'widgets/file_save_format_input.dart';
import 'widgets/gender_input.dart';
import 'widgets/name_input.dart';
import 'widgets/phone_number_input.dart';
import 'widgets/user_save_button.dart';

class EditUserPage extends StatelessWidget {
  const EditUserPage({Key? key, this.user}) : super(key: key);

  final UserModel? user;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<EditUserBloc>(
      create: (context) => EditUserBloc(
        userRepository: context.read<UserRepository>(),
        user: user,
      ),
      child: BlocListener<EditUserBloc, EditUserState>(
        listenWhen: (previous, current) {
          return previous.status != current.status && current.status.isSuccess;
        },
        listener: (context, state) {
          Navigator.pop(context);
        },
        child: const EditUserView(),
      ),
    );
  }
}

class EditUserView extends StatefulWidget {
  const EditUserView({Key? key}) : super(key: key);

  @override
  State<EditUserView> createState() => _EditUserViewState();
}

class _EditUserViewState extends State<EditUserView> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Builder(builder: (context) {
          final isAdd = context.watch<EditUserBloc>().state.isAddUser;
          return Text(isAdd ? 'Tạo người dùng' : 'Cập nhật');
        }),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: ListView(
            children: const [
              NameInput(),
              GenderInput(),
              PhoneNumberInput(),
              BirthdayInput(),
              AddressInput(),
              FileSaveFormatInput(),
            ],
          ),
        ),
      ),
      floatingActionButton: UserSaveButton(formKey: _formKey),
    );
  }
}
