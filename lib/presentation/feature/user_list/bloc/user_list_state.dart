part of 'user_list_bloc.dart';

class UserListState {
  final List<UserModel> jsonUsers;
  final List<UserModel> xmlUsers;
  final UserListStatus status;
  final UserModel? lastUserDeleted;
  final String? errorText;
  final ImportExportStatus importExportStatus;
  final UserListAction action;
  // File exported
  final File? fileExported;
  final String? fileExportedUrl;
  // List after imported
  final List<UserModel>? userListImported;
  // Save file status
  final SaveFileStatus saveFileStatus;

  List<UserModel> get allUserList => [...jsonUsers, ...xmlUsers];

  const UserListState({
    this.jsonUsers = const [],
    this.xmlUsers = const [],
    this.status = UserListStatus.initial,
    this.lastUserDeleted,
    this.errorText,
    this.importExportStatus = ImportExportStatus.initial,
    this.action = UserListAction.none,
    this.fileExported,
    this.fileExportedUrl,
    this.userListImported,
    this.saveFileStatus = SaveFileStatus.none,
  });

  UserListState copyWith({
    List<UserModel>? jsonUsers,
    List<UserModel>? xmlUsers,
    UserListStatus? status,
    UserModel? lastUserDeleted,
    String? errorText,
    ImportExportStatus? importExportStatus,
    UserListAction? action,
    File? fileExported,
    String? fileExportedUrl,
    List<UserModel>? userListImported,
    SaveFileStatus? saveFileStatus,
  }) {
    return UserListState(
      jsonUsers: jsonUsers ?? this.jsonUsers,
      xmlUsers: xmlUsers ?? this.xmlUsers,
      status: status ?? this.status,
      lastUserDeleted: lastUserDeleted ?? this.lastUserDeleted,
      errorText: errorText ?? this.errorText,
      importExportStatus: importExportStatus ?? this.importExportStatus,
      action: action ?? this.action,
      fileExported: fileExported ?? this.fileExported,
      fileExportedUrl: fileExportedUrl ?? this.fileExportedUrl,
      userListImported: userListImported ?? this.userListImported,
      saveFileStatus: saveFileStatus ?? this.saveFileStatus,
    );
  }
}

enum UserListStatus { initial, loading, success, failure, cancel }

extension UserListStatusEx on UserListStatus {
  bool get isInitial => this == UserListStatus.initial;
  bool get isLoading => this == UserListStatus.loading;
  bool get isSuccess => this == UserListStatus.success;
  bool get isFailure => this == UserListStatus.failure;
  bool get isCancel => this == UserListStatus.cancel;
}

enum UserListAction { none, import, export }

extension UserListActionEx on UserListAction {
  bool get isNone => this == UserListAction.none;
  bool get isImport => this == UserListAction.import;
  bool get isExport => this == UserListAction.export;
}

enum SaveFileStatus { none, loading, success, failure }

extension SaveFileStatusEx on SaveFileStatus {
  bool get isNone => this == SaveFileStatus.none;
  bool get isLoading => this == SaveFileStatus.loading;
  bool get isSuccess => this == SaveFileStatus.success;
  bool get isFailure => this == SaveFileStatus.failure;
}
