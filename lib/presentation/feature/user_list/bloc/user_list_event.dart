part of 'user_list_bloc.dart';

abstract class UserListEvent {
  const UserListEvent();
}

class UserListUserGetAll extends UserListEvent {
  const UserListUserGetAll();
}

class UserListUserCreated extends UserListEvent {
  const UserListUserCreated(this.user);

  final UserModel user;
}

class UserListUserRandomCreated extends UserListEvent {
  const UserListUserRandomCreated();
}

class UserListUserDeleted extends UserListEvent {
  const UserListUserDeleted(this.user);

  final UserModel user;
}

class UserListUndoDeletionRequested extends UserListEvent {
  const UserListUndoDeletionRequested();
}

class UserListDataImportRead extends UserListEvent {
  const UserListDataImportRead();
}

class UserListDataExported extends UserListEvent {
  const UserListDataExported();
}

class UserListFileExportedOpened extends UserListEvent {
  const UserListFileExportedOpened();
}

class UserListFileExportedShared extends UserListEvent {
  const UserListFileExportedShared();
}

class UserListAddUserDataFromImported extends UserListEvent {
  const UserListAddUserDataFromImported(this.fileMode);

  final FileMode fileMode;
}

class UserListFileExportedSaved extends UserListEvent {
  const UserListFileExportedSaved();
}

class UserListDataImportByQRCodeRead extends UserListEvent {
  const UserListDataImportByQRCodeRead(this.fileUrl);

  final String fileUrl;
}
