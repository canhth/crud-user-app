import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:file_picker/file_picker.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../data/dummy_data.dart';
import '../../../../data/models/user_model.dart';
import '../../../../data/tsv_file/file_manager.dart';
import '../../../../data/tsv_file/user_tsv_file_manager.dart';
import '../../../../domain/repository/firebase_storage_repository.dart';
import '../../../../domain/repository/user_repository.dart';
import '../../../constants/constants.dart';
import '../../../utils/enums/import_export_status.dart';
import '../../../utils/permission_handler.dart';

part 'user_list_event.dart';
part 'user_list_state.dart';

class UserListBloc extends Bloc<UserListEvent, UserListState> {
  UserListBloc({
    required UserRepository userRepository,
    required UserTsvFileManager userTsvFileManager,
    required FirebaseStorageRepository firebaseStorageRepository,
  })  : _userRepository = userRepository,
        _userTsvFileManager = userTsvFileManager,
        _firebaseStorageRepository = firebaseStorageRepository,
        super(const UserListState()) {
    on<UserListUserGetAll>(_onUserGetAll);
    on<UserListUserCreated>(_onUserCreated);
    on<UserListUserRandomCreated>(_onUserRandomCreated);
    on<UserListUserDeleted>(_onUserDeleted);
    on<UserListUndoDeletionRequested>(_onUndoDeletionRequested);
    on<UserListDataImportRead>(_onDataImportRead);
    on<UserListDataImportByQRCodeRead>(_onDataImportByQRCodeRead);
    on<UserListDataExported>(_onDataExported);
    on<UserListFileExportedOpened>(_onFileExportedOpened);
    on<UserListFileExportedShared>(_onFileExportedShared);
    on<UserListAddUserDataFromImported>(_onAddUserDataFromImported);
    on<UserListFileExportedSaved>(_onAddUserFileExportedSaved);
  }

  final UserRepository _userRepository;
  final UserTsvFileManager _userTsvFileManager;
  final FirebaseStorageRepository _firebaseStorageRepository;

  FutureOr<void> _onUserGetAll(
    UserListUserGetAll event,
    Emitter<UserListState> emit,
  ) async {
    emit(state.copyWith(status: UserListStatus.loading));

    try {
      final jsonUsers = await _userRepository.readUserJsonFile();
      final xmlUsers = await _userRepository.readUserXmlFile();

      emit(
        state.copyWith(
          status: UserListStatus.success,
          jsonUsers: jsonUsers,
          xmlUsers: xmlUsers,
        ),
      );
    } catch (e) {
      emit(state.copyWith(
        status: UserListStatus.failure,
        errorText: e.toString(),
      ));
    }
  }

  FutureOr<void> _onUserCreated(
    UserListUserCreated event,
    Emitter<UserListState> emit,
  ) async {
    emit(state.copyWith(status: UserListStatus.loading));

    try {
      final newUserList = await _addUser(event.user);

      if (event.user.fileSaveFormat.isJson) {
        emit(
          state.copyWith(
            status: UserListStatus.success,
            jsonUsers: newUserList,
          ),
        );
      } else {
        emit(
          state.copyWith(
            status: UserListStatus.success,
            xmlUsers: newUserList,
          ),
        );
      }
    } catch (e) {
      emit(state.copyWith(
        status: UserListStatus.failure,
        errorText: e.toString(),
      ));
    }
  }

  FutureOr<void> _onUserDeleted(
    UserListUserDeleted event,
    Emitter<UserListState> emit,
  ) async {
    emit(state.copyWith(status: UserListStatus.loading));

    try {
      final userDeleted = event.user;

      if (userDeleted.fileSaveFormat.isJson) {
        final newUserList =
            state.jsonUsers.where((user) => user.id != userDeleted.id).toList();

        await _userRepository.writeUserJsonFile(newUserList);

        emit(
          state.copyWith(
            status: UserListStatus.success,
            jsonUsers: newUserList,
            lastUserDeleted: userDeleted,
          ),
        );
      } else {
        final newUserList =
            state.xmlUsers.where((user) => user.id != userDeleted.id).toList();

        await _userRepository.writeUserXmlFile(newUserList);

        emit(
          state.copyWith(
            status: UserListStatus.success,
            xmlUsers: newUserList,
            lastUserDeleted: userDeleted,
          ),
        );
      }
    } catch (e) {
      emit(state.copyWith(
        status: UserListStatus.failure,
        errorText: e.toString(),
      ));
    }
  }

  FutureOr<void> _onUndoDeletionRequested(
    UserListUndoDeletionRequested event,
    Emitter<UserListState> emit,
  ) async {
    emit(state.copyWith(status: UserListStatus.loading));

    try {
      if (state.lastUserDeleted == null) {
        return;
      }

      final user = state.lastUserDeleted!;

      final newUserList = await _addUser(user);

      if (user.fileSaveFormat.isJson) {
        emit(state.copyWith(jsonUsers: newUserList));
      } else {
        emit(state.copyWith(xmlUsers: newUserList));
      }

      emit(state.copyWith(
        status: UserListStatus.success,
        lastUserDeleted: null,
      ));
    } catch (e) {
      emit(state.copyWith(
        status: UserListStatus.failure,
        errorText: e.toString(),
      ));
    }
  }

  FutureOr<void> _onUserRandomCreated(
    UserListUserRandomCreated event,
    Emitter<UserListState> emit,
  ) async {
    emit(state.copyWith(status: UserListStatus.loading));

    try {
      final random = Random();
      final user =
          randomUserList[random.nextInt(randomUserList.length)].copyWith(
        id: DateTime.now().millisecondsSinceEpoch,
        createAt: DateTime.now(),
      );

      final newUserList = await _addUser(user);

      if (user.fileSaveFormat.isJson) {
        emit(state.copyWith(
          status: UserListStatus.success,
          jsonUsers: newUserList,
        ));
      } else {
        emit(state.copyWith(
          status: UserListStatus.success,
          xmlUsers: newUserList,
        ));
      }
    } catch (e) {
      emit(state.copyWith(
        status: UserListStatus.failure,
        errorText: e.toString(),
      ));
    }
  }

  // Return new user list
  Future<List<UserModel>> _addUser(UserModel user) async {
    late final List<UserModel> newUserList;

    try {
      if (user.fileSaveFormat.isJson) {
        newUserList = [...state.jsonUsers, user];
        await _userRepository.writeUserJsonFile(newUserList);
      } else {
        newUserList = [...state.xmlUsers, user];
        await _userRepository.writeUserXmlFile(newUserList);
      }

      return newUserList;
    } catch (e) {
      rethrow;
    }
  }

  FutureOr<void> _onDataImportRead(
    UserListDataImportRead event,
    Emitter<UserListState> emit,
  ) async {
    emit(state.copyWith(
      importExportStatus: ImportExportStatus.loading,
      action: UserListAction.import,
    ));

    try {
      FilePickerResult? result = await FilePicker.platform.pickFiles(
        initialDirectory: Constants.downloadDirectoryPathOnAndroid,
      );

      if (result != null) {
        File file = File(result.files.single.path!);

        if (FileManager().getFileType(file) != 'tsv') {
          return emit(state.copyWith(
            importExportStatus: ImportExportStatus.cancel,
            errorText: 'Chỉ nhận tệp ".tsv"',
          ));
        }

        final userList = await _userTsvFileManager.readTsvFile(file);

        emit(state.copyWith(
          importExportStatus: ImportExportStatus.success,
          userListImported: userList,
        ));
      } else {
        emit(state.copyWith(
          importExportStatus: ImportExportStatus.initial,
        ));
      }
    } catch (e) {
      emit(state.copyWith(
        importExportStatus: ImportExportStatus.failure,
        errorText: e.toString(),
      ));
    }
  }

  FutureOr<void> _onDataExported(
    UserListDataExported event,
    Emitter<UserListState> emit,
  ) async {
    emit(state.copyWith(
      importExportStatus: ImportExportStatus.loading,
      action: UserListAction.export,
    ));

    try {
      final userJsonList = await _userRepository.readUserJsonFile();
      final userXmlList = await _userRepository.readUserXmlFile();
      final allUserList = [...userJsonList, ...userXmlList];

      if (allUserList.isEmpty) {
        return;
      }

      final fileName = 'users_${DateTime.now().millisecondsSinceEpoch}';

      final fileExported = await _userTsvFileManager.createTempTsvFile(
        fileName,
        allUserList,
      );

      final fileExportedUrl = await _firebaseStorageRepository.uploadFile(
        fileExported,
      );

      emit(state.copyWith(
        importExportStatus: ImportExportStatus.success,
        fileExported: fileExported,
        fileExportedUrl: fileExportedUrl,
      ));
    } catch (e) {
      emit(state.copyWith(
        importExportStatus: ImportExportStatus.failure,
        errorText: e.toString(),
      ));
    }
  }

  FutureOr<void> _onFileExportedOpened(
    UserListFileExportedOpened event,
    Emitter<UserListState> emit,
  ) {
    if (state.fileExported != null) {
      FileManager().openFile(state.fileExported!);
    }
  }

  FutureOr<void> _onFileExportedShared(
    UserListFileExportedShared event,
    Emitter<UserListState> emit,
  ) {
    if (state.fileExported != null) {
      FileManager().shareFile(state.fileExported!);
    }
  }

  // Return new user list
  Future<List<UserModel>> _addUserList(List<UserModel> userList) async {
    late final List<UserModel> newUserList;

    try {
      final jsonUserList =
          userList.where((user) => user.fileSaveFormat.isJson).toList();
      final xmlUserList =
          userList.where((user) => user.fileSaveFormat.isXml).toList();

      if (jsonUserList.isNotEmpty) {
        newUserList = [...state.jsonUsers, ...jsonUserList];
        await _userRepository.writeUserJsonFile(newUserList);
      }
      if (xmlUserList.isNotEmpty) {
        newUserList = [...state.xmlUsers, ...xmlUserList];
        await _userRepository.writeUserXmlFile(newUserList);
      }

      return newUserList;
    } catch (e) {
      rethrow;
    }
  }

  FutureOr<void> _onAddUserDataFromImported(
    UserListAddUserDataFromImported event,
    Emitter<UserListState> emit,
  ) async {
    emit(state.copyWith(status: UserListStatus.loading));

    final userListImported = state.userListImported;
    if (userListImported == null) {
      return;
    }

    final jsonUserList =
        userListImported.where((user) => user.fileSaveFormat.isJson).toList();

    final xmlUserList =
        userListImported.where((user) => user.fileSaveFormat.isXml).toList();

    try {
      if (event.fileMode == FileMode.append) {
        final newJsonUserList = [...state.jsonUsers, ...jsonUserList];
        await _userRepository.writeUserJsonFile(newJsonUserList);

        final newXmlUserList = [...state.xmlUsers, ...xmlUserList];
        await _userRepository.writeUserXmlFile(newXmlUserList);

        emit(state.copyWith(
          status: UserListStatus.success,
          jsonUsers: newJsonUserList,
          xmlUsers: newXmlUserList,
          userListImported: null,
        ));
      }

      // Override data
      else {
        _userRepository.writeUserXmlFile(xmlUserList);
        _userRepository.writeUserJsonFile(jsonUserList);

        emit(state.copyWith(
          status: UserListStatus.success,
          jsonUsers: jsonUserList,
          xmlUsers: xmlUserList,
          userListImported: null,
        ));
      }
    } catch (e) {
      emit(state.copyWith(
        status: UserListStatus.failure,
        errorText: e.toString(),
      ));
    }
  }

  FutureOr<void> _onAddUserFileExportedSaved(
    UserListFileExportedSaved event,
    Emitter<UserListState> emit,
  ) async {
    emit(state.copyWith(saveFileStatus: SaveFileStatus.loading));

    try {
      final file = state.fileExported;

      if (file != null) {
        await FileManager().saveFileToDownloadFolder(file);
      }

      emit(state.copyWith(saveFileStatus: SaveFileStatus.success));
    } catch (e) {
      emit(state.copyWith(
        saveFileStatus: SaveFileStatus.failure,
        errorText: e.toString(),
      ));
    }
  }

  FutureOr<void> _onDataImportByQRCodeRead(
    UserListDataImportByQRCodeRead event,
    Emitter<UserListState> emit,
  ) async {
    emit(state.copyWith(
      importExportStatus: ImportExportStatus.loading,
      action: UserListAction.import,
    ));

    /// Check storage permission
    await PermissionHandler().checkStoragePermission();

    try {
      File file = await FileManager().downloadFile(event.fileUrl);

      if (!file.existsSync()) {
        return emit(state.copyWith(
          importExportStatus: ImportExportStatus.cancel,
          errorText: 'File không tồn tại',
        ));
      }

      if (FileManager().getFileType(file) != 'tsv') {
        return emit(state.copyWith(
          importExportStatus: ImportExportStatus.cancel,
          errorText: 'Chỉ nhận tệp ".tsv"',
        ));
      }

      final userList = await _userTsvFileManager.readTsvFile(file);

      emit(state.copyWith(
        importExportStatus: ImportExportStatus.success,
        userListImported: userList,
      ));
    } catch (e) {
      emit(state.copyWith(
        importExportStatus: ImportExportStatus.failure,
        errorText: e.toString(),
      ));
    }
  }
}
