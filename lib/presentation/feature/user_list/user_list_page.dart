import 'dart:io';

import 'package:crud_user_app/presentation/utils/enums/import_export_status.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/tsv_file/user_tsv_file_manager.dart';
import '../../../domain/repository/firebase_storage_repository.dart';
import '../../../domain/repository/user_repository.dart';
import 'bloc/user_list_bloc.dart';
import 'widgets/qr_code_container.dart';
import 'widgets/user_list_view.dart';

class UserListPage extends StatelessWidget {
  const UserListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => UserListBloc(
        userRepository: context.read<UserRepository>(),
        userTsvFileManager: context.read<UserTsvFileManager>(),
        firebaseStorageRepository: context.read<FirebaseStorageRepository>(),
      )..add(const UserListUserGetAll()),
      child: MultiBlocListener(
        listeners: [
          BlocListener<UserListBloc, UserListState>(
            listenWhen: (previous, current) {
              return previous.status != current.status ||
                  previous.importExportStatus != current.importExportStatus;
            },
            listener: (context, state) {
              if (state.status.isFailure ||
                  state.importExportStatus.isFailure) {
                final errorText = state.errorText ?? 'Thao tác lỗi.';

                ScaffoldMessenger.of(context)
                  ..hideCurrentSnackBar()
                  ..showSnackBar(
                    SnackBar(content: Text('Error: $errorText')),
                  );
              } else if (state.importExportStatus.isCancel &&
                  state.errorText != null) {
                ScaffoldMessenger.of(context)
                  ..hideCurrentSnackBar()
                  ..showSnackBar(
                    SnackBar(content: Text('Error: ${state.errorText}')),
                  );
              }
            },
          ),
          BlocListener<UserListBloc, UserListState>(
            listenWhen: (previous, current) {
              return previous.lastUserDeleted != current.lastUserDeleted &&
                  current.lastUserDeleted != null;
            },
            listener: (context, state) {
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    content: const Text('Đã xoá thành công.'),
                    action: SnackBarAction(
                      label: 'Hoàn tác',
                      onPressed: () {
                        context
                            .read<UserListBloc>()
                            .add(const UserListUndoDeletionRequested());
                      },
                    ),
                  ),
                );
            },
          ),
          BlocListener<UserListBloc, UserListState>(
            listenWhen: (previous, current) {
              return previous.importExportStatus !=
                      current.importExportStatus &&
                  current.importExportStatus.isSuccess;
            },
            listener: (context, state) {
              if (state.action.isExport) {
                _showModelBottomSheet(context, state.fileExportedUrl!);
              }

              if (state.action.isImport) {
                _showOptionDialog(context);
              }
            },
          ),
          BlocListener<UserListBloc, UserListState>(
            listenWhen: (previous, current) {
              return previous.saveFileStatus != current.saveFileStatus;
            },
            listener: (context, state) {
              if (state.saveFileStatus.isFailure) {
                final errorText = state.errorText ?? 'Thao tác lỗi.';

                ScaffoldMessenger.of(context)
                  ..hideCurrentSnackBar()
                  ..showSnackBar(
                    SnackBar(content: Text('Error: $errorText')),
                  );
              } else if (state.saveFileStatus.isSuccess) {
                ScaffoldMessenger.of(context)
                  ..hideCurrentSnackBar()
                  ..showSnackBar(
                    const SnackBar(content: Text('Lưu file thành công')),
                  );
              }
            },
          ),
        ],
        child: BlocBuilder<UserListBloc, UserListState>(
          builder: (context, state) {
            return IgnorePointer(
              ignoring: state.status.isLoading,
              child: const UserListView(),
            );
          },
        ),
      ),
    );
  }

  void _showModelBottomSheet(BuildContext context, String qrCodeData) {
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      builder: (ctx) => Padding(
        padding: const EdgeInsets.only(top: 20, bottom: 40),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            QRCodeContainer(qrCodeData: qrCodeData),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FilledButton(
                  onPressed: () {
                    context
                        .read<UserListBloc>()
                        .add(const UserListFileExportedSaved());
                    Navigator.pop(context);
                  },
                  child: const Text('Save file'),
                ),
                const SizedBox(width: 20),
                FilledButton(
                  onPressed: () {
                    context
                        .read<UserListBloc>()
                        .add(const UserListFileExportedShared());
                  },
                  child: const Text('Share file'),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  void _showOptionDialog(BuildContext context) {
    showDialog<void>(
      context: context,
      builder: (ctx) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const Padding(
                  padding: EdgeInsets.only(
                    top: 8,
                    bottom: 12,
                  ),
                  child:
                      Text('Bạn muốn ghi đè hay bổ sung vào danh sách hiện có'),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FilledButton(
                      onPressed: () {
                        context.read<UserListBloc>().add(
                              const UserListAddUserDataFromImported(
                                FileMode.write,
                              ),
                            );
                        Navigator.pop(context);
                      },
                      child: const Text('Ghi đè'),
                    ),
                    const SizedBox(width: 20),
                    FilledButton(
                      onPressed: () {
                        context.read<UserListBloc>().add(
                              const UserListAddUserDataFromImported(
                                FileMode.append,
                              ),
                            );
                        Navigator.pop(context);
                      },
                      child: const Text('Bổ sung'),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
