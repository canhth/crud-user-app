import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

class QRCodeContainer extends StatelessWidget {
  const QRCodeContainer({
    Key? key,
    required this.qrCodeData,
  }) : super(key: key);

  final String qrCodeData;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 220,
      height: 220,
      margin: const EdgeInsets.symmetric(vertical: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        gradient: const LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color(0xff30c8ee),
            Color(0xffFDB100),
          ],
        ),
      ),
      alignment: Alignment.center,
      child: Container(
        height: 200,
        width: 200,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.white,
          // gradient:
        ),
        alignment: Alignment.center,
        child: QrImage(
          data: qrCodeData,
          version: QrVersions.auto,
          errorStateBuilder: (ctx, error) => const Text(
            "Uh oh! Something went wrong...",
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}
