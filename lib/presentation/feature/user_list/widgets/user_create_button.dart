import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../edit_user/edit_user_page.dart';
import '../bloc/user_list_bloc.dart';

class UserCreateButton extends StatelessWidget {
  const UserCreateButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () async {
        await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => const EditUserPage(),
          ),
        );

        if (context.mounted) {
          context.read<UserListBloc>().add(const UserListUserGetAll());
        }
      },
      child: const Icon(Icons.add),
    );
  }
}
