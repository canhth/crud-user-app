import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/user_list_bloc.dart';

class UserRandomCreateButton extends StatelessWidget {
  const UserRandomCreateButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return FilledButton(
      onPressed: () async {
        context.read<UserListBloc>().add(const UserListUserRandomCreated());
      },
      child: const Text('Tạo ngẫu nhiên'),
    );
  }
}
