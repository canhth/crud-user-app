import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../data/models/user_model.dart';
import '../../qr_code_scanner/qr_code_scanner_screen.dart';
import '../../user_detail/user_detail_page.dart';
import '../bloc/user_list_bloc.dart';
import 'ie_data_button.dart';
import 'user_create_button.dart';
import 'user_list_tile.dart';
import 'user_random_create_button.dart';

class UserListView extends StatelessWidget {
  const UserListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Danh sách người dùng'),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(80),
          child: Column(
            children: [
              Wrap(
                children: [
                  /// Import file by QR code
                  IEDataButton(
                    onTap: () {
                      _importByQRCode(context, (fileUrl) {
                        context
                            .read<UserListBloc>()
                            .add(UserListDataImportByQRCodeRead(fileUrl));
                      });
                    },
                    title: 'Import by QR',
                  ),
                  const SizedBox(width: 20),

                  /// Import file button
                  IEDataButton(
                    onTap: () {
                      context
                          .read<UserListBloc>()
                          .add(const UserListDataImportRead());
                    },
                    title: 'Import file',
                  ),
                  const SizedBox(width: 20),

                  /// Export file button
                  IEDataButton(
                    onTap: () {
                      context
                          .read<UserListBloc>()
                          .add(const UserListDataExported());
                    },
                    title: 'Export file',
                  ),
                  const SizedBox(width: 20),
                ],
              ),
              const SizedBox(height: 10),
              const Text(
                'File được Import/Export chỉ hỗ trợ định dạng ".tsv"',
                style: TextStyle(color: Colors.white),
              ),
              const SizedBox(height: 10),
            ],
          ),
        ),
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          context.read<UserListBloc>().add(const UserListUserGetAll());
        },
        child: const _Body(),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: const [
          UserRandomCreateButton(),
          SizedBox(width: 10),
          UserCreateButton(),
        ],
      ),
    );
  }

  Future<void> _importByQRCode(
    BuildContext context,
    void Function(String fileUrl) callback,
  ) async {
    final String? fileUrl = await Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => const QRCodeScannerPage(),
    ));

    if (fileUrl != null && fileUrl.isNotEmpty) {
      callback.call(fileUrl);
    }
  }
}

class _Body extends StatelessWidget {
  const _Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserListBloc, UserListState>(
      builder: (context, state) {
        if (state.allUserList.isEmpty) {
          if (state.status.isLoading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state.status.isFailure) {
            final errorText = state.errorText ?? '';
            return Center(
              child: Text('Error: $errorText'),
            );
          }
          return const Center(
            child: Text('Danh sách trống'),
          );
        }

        final users = state.allUserList;

        // Sort the list of users by latest creation time
        users.sort((u1, u2) => u2.createAt.compareTo(u1.createAt));

        return ListView.builder(
          itemBuilder: (context, index) {
            final UserModel user = users[index];

            return UserListTile(
              user: user,
              backgroundColor: index.isEven ? Colors.blue.shade50 : null,
              onTap: () async {
                await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => UserDetailPage(user: user),
                  ),
                );

                if (context.mounted) {
                  context.read<UserListBloc>().add(const UserListUserGetAll());
                }
              },
              onDeleteTap: () {
                context.read<UserListBloc>().add(UserListUserDeleted(user));
              },
            );
          },
          itemCount: state.allUserList.length,
        );
      },
    );
  }
}
