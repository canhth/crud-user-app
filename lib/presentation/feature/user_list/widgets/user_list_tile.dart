import 'package:crud_user_app/data/models/user_model.dart';
import 'package:flutter/material.dart';

class UserListTile extends StatelessWidget {
  const UserListTile({
    Key? key,
    required this.user,
    required this.backgroundColor,
    required this.onTap,
    required this.onDeleteTap,
  }) : super(key: key);

  final UserModel user;
  final Color? backgroundColor;
  final VoidCallback onTap;
  final VoidCallback onDeleteTap;

  @override
  Widget build(BuildContext context) {
    final userInforText =
        'Giới tính: ${user.gender.text}\nSĐT: ${user.phoneNumber}';

    return ListTile(
      title: Text(user.name),
      subtitle: Text(userInforText),
      isThreeLine: true,
      trailing: IconButton(
        onPressed: onDeleteTap,
        icon: const Icon(Icons.delete),
      ),
      tileColor: backgroundColor,
      onTap: onTap,
      leading: Text(user.fileSaveFormat.name),
    );
  }
}
