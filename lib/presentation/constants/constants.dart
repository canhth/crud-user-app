class Constants {
  static const tsvRecordSeparatorCharacter = '\r\n';
  static const downloadDirectoryPathOnAndroid = "/storage/emulated/0/Download";
}
